﻿using Autofac;
using TestApp.ViewModel;

namespace TestApp.Startup
{
	public class Botstrapper
	{
		public IContainer Bootstrap()
		{
			var builder = new ContainerBuilder();

			builder.RegisterType<MainWindow>().AsSelf();
			builder.RegisterType<MainViewModel>().AsSelf();

			return builder.Build();
		}
	}
}
