﻿using Autofac;
using LoggingAPI;
using System;
using System.Windows;
using System.Windows.Threading;
using TestApp.Startup;

namespace TestApp
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private void Application_Startup(object sender, StartupEventArgs e)
		{
			var bootstrapp = new Botstrapper();

			try
			{
				var container = bootstrapp.Bootstrap();

				var mainWindow = container.Resolve<MainWindow>();
				mainWindow.Show();
			}
			catch (Exception ex)
			{
				Logger.Error("Application can't start. See the log information!", ex, Level.Fatal);
				MessageBox.Show("Application can't start. See the log information!");
				Application.Current.Shutdown();
			}
		}

		private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			Logger.Error("Unexpected problem!", e.Exception, Level.Fatal);
			MessageBox.Show($"Unexpected error!See the log information!{Environment.NewLine}Error Message: {e.Exception.Message}", "Error!");
			e.Handled = true;
		}
	}
}
